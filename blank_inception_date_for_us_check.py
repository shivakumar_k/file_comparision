import requests
import csv
import json
from datetime import date 
from datetime import timedelta, datetime
from operator import itemgetter

pipeline_base_url = 'http://159.65.226.37:8007'
pipeline_auth = {'Authorization':'token 17fc82e650d62d131aad7af93732236dd45f61d2','pipeline-token':'8d076268-00a2-4e55-a3ed-5cce86aac65c','Content-Type':'application/json'}
coredata_base_url = 'http://159.65.226.37:8007'
coredata_auth = {'Authorization':'token 17fc82e650d62d131aad7af93732236dd45f61d2','pipeline-token':'8d076268-00a2-4e55-a3ed-5cce86aac65c','Content-Type':'application/json'}

mismatched_market_data_list = []

get_firstbridge_ids_url = pipeline_base_url+"/api/v1.0/get_etf_universe_by_filter_data"
update_etf_by_fid_url = coredata_base_url+"/api/v1.0/etf/update/"

def getEtfDataAndCompare(url):
    post_data = '{"listing_region":["US"],"asset_class": [],"market_cap_range": [],"commodity_types": []}'
    post_data = json.loads(post_data)
    etf_response = requests.post(url,json=post_data, headers=pipeline_auth)
    onlyResult = etf_response.json()
    your_keys = ['firstbridge_id', 'composite_ticker', 'bloomberg_symbol', 'original_inception_date', 'inception_date']
    dict_you_want = []
    for old_dict in onlyResult['etfs']:
#         filtered_dict = { your_key: old_dict[your_key] for your_key in your_keys }
        if not old_dict['inception_date'] or old_dict['inception_date'] == None:
            dict_you_want.append(old_dict)
    return dict_you_want

etf_list = getEtfDataAndCompare(get_firstbridge_ids_url)

def updateEtfWithInceptionDate(url, etf_list):
    for etf in etf_list:
        etf['inception_date']=etf['original_inception_date']
        # print(etf)
        update_response = requests.post(url+etf['firstbridge_id'],json=etf, headers=coredata_auth)
        finalResult = update_response.json()

updateEtfWithInceptionDate(update_etf_by_fid_url, etf_list)