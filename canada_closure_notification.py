import requests
import csv


cfra_base_url = "http://localhost:8000/api/v1.0"
cfra_header = {
    "Authorization": "Token 3390d8f53b285d00e499c88410ddab7ebe4acca7"}
ca_etfuniverse_file_location = "etfuniversal_1.csv"

# read csv file
existing_etf_list = {}
new_etf_list = []
mismatched_etf_list = []

with open(ca_etfuniverse_file_location, 'r') as existing_file:
    efile_reader = csv.DictReader(existing_file)
    for line in efile_reader:
        try:
            this_row = {}
            this_row["firstbridge_id"] = line["firstbridge_id"]
            this_row["bloomberg_symbol"] = line["bloomberg_symbol"]
            this_row["is_live_listed"] = line["is_live_listed"]
            existing_etf_list[line["firstbridge_id"]] = line["is_live_listed"]
            existing_etf_list[line["firstbridge_id"]+'row'] = this_row
        except:
            pass

etf_response = requests.get(cfra_base_url+"/etf_list_by_filter?listing_country_code=CA ", headers=cfra_header)
onlyResult = etf_response.json()
latest_etf_data = onlyResult['results']

for line in latest_etf_data:
    try:
        this_row = {}
        this_row["firstbridge_id"] = line["firstbridge_id"]
        this_row["bloomberg_symbol"] = line["bloomberg_symbol"]
        this_row["is_live_listed"] = line["is_live_listed"]
        if existing_etf_list[this_row["firstbridge_id"]] == 'TRUE' and this_row["is_live_listed"] == False:
            new_etf_list.append(this_row)
            mismatched_etf_list.append(this_row)
        else:
            new_etf_list.append(
                existing_etf_list[this_row["firstbridge_id"]+'row'])
    except:
        pass

# TODO - Need to update the new data into existing csv file
new_csv_with_data = open(ca_etfuniverse_file_location, 'w+', newline='')
keys = new_etf_list[0].keys()
dict_writer = csv.DictWriter(new_csv_with_data, keys)
dict_writer.writeheader()
dict_writer.writerows(new_etf_list)
new_csv_with_data.close()


# print('Mismatchecd ETF List for sending alert')
print(mismatched_etf_list)


class Notifications:

    def __init__(self, base_url, auth_token, notification_head, notification_body, to_user):
        self.head = notification_head
        self.body = notification_body
        self.user = to_user
        self.auth_token = auth_token
        self.base_url = base_url

    def send2cms(self):
        payload = {
            "head": self.head,
            "body": self.body,
            "to_user": self.user
        }
        headers = {
            "Authorization": "Token " + self.auth_token
        }
        resp_raw = requests.post(self.base_url + "/api/v1.0/notification/create", json=payload, headers=headers,
                                 timeout=3)
        resp = resp_raw.json()
        return resp

# TODO - Need to call the create alert call to notify the user
# Notifications(cfra_base_url, TOKEN, CRONENAME, mismatched_etf_list, OWNERID).send2cms()